/**
 * Copyright (c) 2014, TRIOLOGY GmbH All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer. 2. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * https://www.scm-manager.com
 *
 */
Ext.ns('Triology.cas');

Triology.cas.I18n = {

  titleText : 'CAS Authentication',

  toleranceText : 'Time Tolerance',
  toleranceHelpText : 'Time tolerance in (ms) enforced during validation, to allow for time drifting between the SCM-Manager server and the CAS server.',

  casServerUrlText : 'CAS Server URL',
  casServerUrlHelpText : 'CAS Server URL. You have to restart your SCM-Manager after changing this value',

  enabledText : 'Enabled',
  enabledHelpText : 'If "Enabled" SCM-Manager uses CAS for authentication. "Disabled" allows convential authentication.',

  casAttrUsernameText : 'Username',
  casAttrUsernameHelpText : 'CAS attribute for username',

  casAttrDisplayNameText : 'Display name',
  casAttrDisplayNameHelpText : 'CAS attribute for display name',

  casAttrMailText : 'Email',
  casAttrMailHelpText : 'CAS attribute for email',

  casAttrGroupText : 'Group',
  casAttrGroupHelpText : 'CAS attribute for group',

  lblCasAttributesText : 'CAS-Attributes:',

  testConnectionTitleText: 'CAS Connection Test',
  testConnectionText: 'Test Connection',

  loadingText : 'Loading ...',
  submitText : 'Submit ...',
  logoutRedirectText : 'Single-Sign-Out in progress...',

  msgTitleText : 'Message',

  // errors
  errorBoxTitle : 'Error',
  errorOnSubmitText : 'Check your entries.',
  errorOnLoadText : 'Error during config load.',
  errorOnReloadText : 'Error during reload.',
  errorOnLoadLogoutUrlText : 'Error during load logout URL.',
  errorValidSubmitText : 'Make sure that your entry is valid.',

  // test connection
  usernameText: 'Username',
  passwordText: 'Password',
  testText: 'Test',
  cancelText: 'Cancel',
  waitTitleText: 'Connecting',
  waitMsgText: 'Sending data...',
  failedMsgText: 'CAS Connection Test failed!',

  connectionText: 'Connection',
  authenticateUserText: 'Authenticate user',
  ticketValidText: 'Ticket valid',
  userValidText: 'Returned user is valid',

  userNameText: 'Name',
  displayNameText: 'Display Name',
  mailText: 'Mail',
  userText: 'User',
  groupsText: 'Groups',

  disableSSLVerificationText: 'Disable SSL Verification',
  disableSSLVerificationHelpText: 'Disable SSL certificate verification. \n\
    <b>Warning: </b> Do not use this setting in production. \n\
    This setting is only for development needs.'

};

// German translation

if ('de' === i18n.country) {
  
  Triology.cas.I18n = {

    titleText : 'CAS Authentifizierung',

    toleranceText : 'Toleranzzeit',
    toleranceHelpText : 'Toleranzzeit während der Validierung für die Zeitunterschiede zwischen SCM-Manager-Server und den CAS-Server. SCM-Manager muss nach einer Änderung neu gestartet werden',

    casServerUrlText : 'CAS Server URL',
    casServerUrlHelpText : 'CAS Server URL. SCM-Manager muss nach einer Änderung neu gestartet werden',

    enabledText : 'Aktiv',
    enabledHelpText : 'Wenn "Aktiv" verwendet SCM-Manager CAS Authentifizierung. Ist das Plugin deaktiviert verwendet SCM-Manager den herkömmlichen Login.',

    casAttrUsernameText : 'Benutzername',
    casAttrUsernameHelpText : 'CAS-Attribut für den Benutzernamen',

    casAttrDisplayNameText : 'Anzeigename',
    casAttrDisplayNameHelpText : 'CAS-Attribut für den Anzeigenamen',

    casAttrMailText : 'E-Mail',
    casAttrMailHelpText : 'CAS-Attribut für die E-Mail-Adresse',

    casAttrGroupText : 'Gruppen',
    casAttrGroupHelpText : 'CAS-Attribut für Gruppen',

    lblCasAttributesText : 'CAS-Attribute:',

    testConnectionTitleText: 'CAS Anmeldung prüfen',
    testConnectionText: 'Anmeldung prüfen',

    loadingText : 'Laden ...',
    submitText : 'Senden ...',
    logoutRedirectText : 'Single-Sign-Out wird durchgeführt...',

    msgTitleText : 'Meldung',

    // errors
    errorBoxTitle : 'Fehler',
    errorOnSubmitText : 'Prüfen Sie ob die angegebenen Daten existieren.',
    errorOnLoadText : 'Fehler beim Laden der Konfigurations-Werte.',
    errorOnReloadText : 'Fehler beim Laden der Datei.',
    errorOnLoadLogoutUrlText : 'Fehler beim Laden der Logout-URL.',
    errorValidSubmitText : 'Bitte Eingaben auf Korrektheit überprüfen.',

    // test connection
    usernameText: 'Benutzername',
    passwordText: 'Passwort',
    testText: 'Prüfen',
    cancelText: 'Abbrechen',
    waitTitleText: 'Verbinden...',
    waitMsgText: 'Daten senden...',
    failedMsgText: 'CAS Anmeldungsprüfung fehlgeschlagen!',

    connectionText: 'Verbindung',
    authenticateUserText: 'Benutzer anmelden',
    ticketValidText: 'Ticket validieren',
    userValidText: 'Benutzer validieren',

    userNameText: 'Name',
    displayNameText: 'Anzeige Name',
    mailText: 'E-Mail',
    userText: 'Benuter',
    groupsText: 'Gruppen',

    disableSSLVerificationText: 'SSL Validierung deaktivieren',
    disableSSLVerificationHelpText: 'Deaktivieren der SSL Zertifikate. \n\
      <b>Warnung: </b> Diese Einstellung sollte nicht für produktive Umgebungen verwendet werden. \n\
      Diese Einstellung sollte nur für die Entwicklung verwendet werden.'
  };
}
