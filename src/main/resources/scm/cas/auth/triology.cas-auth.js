/**
 * Copyright (c) 2014, TRIOLOGY GmbH All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer. 2. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */
Ext.ns('Triology.cas');

// handle cas redirect on ajax request

Ext.Ajax.on('requestexception', function(conn, response){
  if (response.status === 401){
    var casLocation = response.getResponseHeader('X-CAS-Redirect');
    if (casLocation){
      window.location = casLocation;
    }
  }
}, this);

// cas config panel

Triology.cas.GlobalConfigPanel = Ext.extend(Sonia.config.ConfigForm, {

	initComponent : function() {
		var config = {
			title : Triology.cas.I18n.titleText,
			items : [{
                  id : 'enabled',
                  name : 'enabled',
                  xtype : 'checkbox',
                  inputValue : 'true',
                  fieldLabel : Triology.cas.I18n.enabledText,
                  helpText : Triology.cas.I18n.enabledHelpText,
                  listeners : {
                      check : {
                          scope : this,
                          fn : this.dis_enableConfig
                      }
                  }
              }, {
                  id : 'tolerance',
                  name : 'tolerance',
                  xtype : 'textfield',
                  fieldLabel : Triology.cas.I18n.toleranceText,
                  helpText : Triology.cas.I18n.toleranceHelpText,
                  allowBlank : false
              }, {
                  id : 'casServerUrl',
                  name : 'casServerUrl',
                  xtype : 'textfield',
                  fieldLabel : Triology.cas.I18n.casServerUrlText,
                  helpText : Triology.cas.I18n.casServerUrlHelpText,
                  allowBlank : false
              }, {
                  xtype : 'spacer',
                  height : 20
              }, {
                  xtype : 'label',
                  text : Triology.cas.I18n.lblCasAttributesText,
                  style : 'font-weight:bold;'
              }, {
                  xtype : 'spacer',
                  height : 10
              }, {
                  id : 'casAttrUsername',
                  name : 'casAttrUsername',
                  xtype : 'textfield',
                  fieldLabel : Triology.cas.I18n.casAttrUsernameText,
                  helpText : Triology.cas.I18n.casAttrUsernameHelpText,
                  allowBlank : false
              }, {
                  id : 'casAttrDisplayName',
                  name : 'casAttrDisplayName',
                  xtype : 'textfield',
                  fieldLabel : Triology.cas.I18n.casAttrDisplayNameText,
                  helpText : Triology.cas.I18n.casAttrDisplayNameHelpText,
                  allowBlank : false
              }, {
                  id : 'casAttrMail',
                  name : 'casAttrMail',
                  xtype : 'textfield',
                  fieldLabel : Triology.cas.I18n.casAttrMailText,
                  helpText : Triology.cas.I18n.casAttrMailHelpText,
                  allowBlank : false
              }, {
                  id : 'casAttrGroup',
                  name : 'casAttrGroup',
                  xtype : 'textfield',
                  fieldLabel : Triology.cas.I18n.casAttrGroupText,
                  helpText : Triology.cas.I18n.casAttrGroupHelpText,
                  allowBlank : false
              }, {
                  id : 'disableSSLVerification',
                  name : 'disableSSLVerification',
                  xtype : 'checkbox',
                  inputValue: 'true',
                  fieldLabel : Triology.cas.I18n.disableSSLVerificationText,
                  helpText : Triology.cas.I18n.disableSSLVerificationHelpText
              },{
                xtype: 'button',
                fieldLabel : Triology.cas.I18n.testConnectionText,
                name: 'testConnection',
                text: Triology.cas.I18n.testConnectionText,
                scope: this,
                handler: this.testConnection
              }
            ]};

		Ext.apply(this, Ext.apply(this.initialConfig, config));
		Triology.cas.GlobalConfigPanel.superclass.initComponent.apply(this,
				arguments);
	},
  
  testConnection: function(){
    var window = new Triology.cas.ConnectionTestWindow({
      data: this.getForm().getValues()
    });
    window.show();
  },

	onSubmit : function(values) {
		var form = this.getForm();
		if (form.isValid()) {
			this.el.mask(Triology.cas.I18n.submitText);
			Ext.Ajax.request({
						url : restUrl + 'config/auth/cas.json',
						method : 'POST',
						jsonData : values,
						scope : this,
						disableCaching : true,
						success : function(response) {
							this.el.unmask();
						},
						failure : function() {
							this.el.unmask();
							Ext.MessageBox.show({
										title : Triology.cas.I18n.errorBoxTitle,
										msg : Triology.cas.I18n.errorOnSubmitText,
										buttons : Ext.MessageBox.OK,
										icon : Ext.MessageBox.ERROR
									});
						}
					});
		} else {
			// TODO: mask the save button.
			Ext.MessageBox.show({
						title : Triology.cas.I18n.errorBoxTitle,
						msg : Triology.cas.I18n.errorValidSubmitText,
						buttons : Ext.MessageBox.OK,
						icon : Ext.MessageBox.ERROR
					});
		}
	},

	onLoad : function(el) {
		var tid = setTimeout(function() {
					el.mask(Triology.cas.I18n.loadingText,"x-mask-loading");
				}, 100);
		Ext.Ajax.request({
					url : restUrl + 'config/auth/cas.json',
					method : 'GET',
					scope : this,
					disableCaching : true,
					success : function(response) {
						var obj = Ext.decode(response.responseText);
						this.load(obj);
						this.dis_enableConfig(Ext.getCmp('enabled'));
						clearTimeout(tid);
						el.unmask();
					},
					failure : function() {
						clearTimeout(tid);
						el.unmask();
						Ext.MessageBox.show({
									title : Triology.cas.I18n.errorBoxTitle,
									msg : Triology.cas.I18n.errorOnLoadText,
									buttons : Ext.MessageBox.OK,
									icon : Ext.MessageBox.ERROR
								});
					}
				});
	},

	dis_enableConfig : function(checkbox) {
		var cmps = [
          Ext.getCmp('casServerUrl'), Ext.getCmp('tolerance'), 
          Ext.getCmp('casAttrUsername'), Ext.getCmp('casAttrDisplayName'), 
          Ext.getCmp('casAttrMail'), Ext.getCmp('casAttrGroup'), 
          Ext.getCmp('disableSSLVerification')
        ];
		Ext.each(cmps, function(cmp) {
          var checked = checkbox.getValue();
          // Add/remove CSS class which indicates disabling.
          if (!checked) {
              cmp.addClass('x-item-disabled');
          } else {
              cmp.removeClass('x-item-disabled');
          }
          cmp.setReadOnly(!checked);
        });
	}
});

// Override main logout function
Sonia.scm.Main.prototype.casOriglogout = Sonia.scm.Main.prototype.logout;

Ext.override(Sonia.scm.Main, {
  
  logout : function() {
	var el = Ext.getBody();
	var tid = setTimeout(function() {
      el.mask(Triology.cas.I18n.logoutRedirectText,"x-mask-loading");
	}, 100);
	Ext.Ajax.request({
      url : restUrl + 'cas/logout',
      method : 'GET',
      scope : this,
      disableCaching : true,
      success : function(response) {
        var logoutUrl = response.responseText;
        if (logoutUrl !== null) {
          window.location = logoutUrl;
        } else {
          // relaod page if no logout url returned
          location.repload();
        }
        clearTimeout(tid);
        el.unmask();
      },
      failure : function() {
        clearTimeout(tid);
        el.unmask();
        Ext.MessageBox.show({
          title : Triology.cas.I18n.errorBoxTitle,
          msg : Triology.cas.I18n.errorOnLoadLogoutUrlText,
          buttons : Ext.MessageBox.OK,
          icon : Ext.MessageBox.ERROR
        });
      }
    });
  }
});

Ext.reg("casGlobalConfigPanel", Triology.cas.GlobalConfigPanel);

// register global config panel
registerGeneralConfigPanel({
  id : 'casGlobalConfigPanel',
  xtype : 'casGlobalConfigPanel'
});