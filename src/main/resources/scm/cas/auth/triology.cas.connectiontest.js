/**
 * Copyright (c) 2014, TRIOLOGY GmbH All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer. 2. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */
Ext.ns('Triology.cas');
Triology.cas.ConnectionTestForm = Ext.extend(Ext.FormPanel,{
  
  data: null,

  initComponent: function(){

    var config = {
      labelWidth: 100,
      frame: false,
      defaultType: 'textfield',
      monitorValid: true,
      bodyCssClass: 'x-panel-mc',
      bodyStyle: 'padding: 5px',
      listeners: {
        afterrender: function(){
          Ext.getCmp('casTestUsername').focus(true, 500);
        }
      },
      items:[{
        id: 'casTestUsername',
        fieldLabel: Triology.cas.I18n.usernameText,
        name: 'username',
        allowBlank:false,
        listeners: {
          specialkey: {
            fn: this.specialKeyPressed,
            scope: this
          }
        }
      },{
        fieldLabel: Triology.cas.I18n.passwordText,
        name: 'password',
        inputType: 'password',
        allowBlank: false,
        listeners: {
          specialkey: {
            fn: this.specialKeyPressed,
            scope: this
          }
        }
      },{
        id: 'casTestResultPanel',
        xtype: 'panel',
        bodyCssClass: 'x-panel-mc',
        tpl: new Ext.XTemplate([
          '<p style="padding: 0 10px">',
          '  {bind}<span style="padding: 0 5px">&nbsp;</span><b>{Triology.cas.I18n.connectionText}</b><br />',
          '  {authenticateUser}<span style="padding: 0 5px">&nbsp;</span><b>{Triology.cas.I18n.authenticateUserText}</b><br />',
          '  {ticketValid}<span style="padding: 0 5px">&nbsp;</span><b>{Triology.cas.I18n.ticketValidText}</b><br />',
          '  {userValid}<span style="padding: 0 5px">&nbsp;</span><b>{Triology.cas.I18n.userValidText}</b><br />',
          '  <tpl if="exception">',
          '    <b>Exception:</b> {exception}<br />',
          '  </tpl>',
          '  <tpl if="user">',
          '    <br />',
          '    <b>{Triology.cas.I18n.userText}:</b><br />',
          '    <tpl for="user">',
          '      - {Triology.cas.I18n.userNameText}:  {name}<br />',
          '      - {Triology.cas.I18n.displayNameText}:  {displayName}<br />',
          '      - {Triology.cas.I18n.mailText}:  {mail}<br />',
          '    </tpl>',
          '  </tpl>',
          '  <tpl if="groups">',
          '    <br />',
          '    <b>{Triology.cas.I18n.groupsText}:</b><br />',
          '    <tpl for="groups">',
          '      - {.}<br />',
          '    </tpl>',
          '  </tpl>',
          '</p>'
        ])
      }],
      buttons:[{
        text: Triology.cas.I18n.cancelText,
        scope: this,
        handler: this.cancel
      },{
        text: Triology.cas.I18n.testText,
        formBind: true,
        scope: this,
        handler: this.testConnection
      }]
    };

    this.addEvents('cancel');

    Ext.apply(this, Ext.apply(this.initialConfig, config));
    Triology.cas.ConnectionTestForm.superclass.initComponent.apply(this, arguments);
  },

  cancel: function(){
    this.fireEvent('cancel');
  },
  
  getBooleanLabel: function(value){
    var label = null;
    if (value){
      label = '<span style="color: green">&#10004;</span>';
    } else {
      label = '<span style="color: red">&#10008;</span>';
    }
    return label;
  },
  
  showResult: function(data){
    if (debug){
      console.debug('connection test results:');
      console.debug(data);
    }
    data.bind = this.getBooleanLabel(data.bind);
    data.ticketValid = this.getBooleanLabel(data.ticketValid);
    data.authenticateUser = this.getBooleanLabel(data.authenticateUser);
    data.userValid = this.getBooleanLabel(data.userValid);
    
    var resultPanel = Ext.getCmp('casTestResultPanel');
    resultPanel.tpl.overwrite(resultPanel.body, data);
  },

  testConnection: function(){
    var form = this.getForm();
    var testConfig = form.getValues();
    testConfig['cas-config'] = this.data;
    
    this.el.mask(Triology.cas.I18n.submitText);
    Ext.Ajax.request({
      url: restUrl + 'config/auth/cas/test.json',
      method: 'POST',
      jsonData: testConfig,
      scope: this,
      disableCaching: true,
      success: function(response){
        this.el.unmask();
        if ( debug ){
          console.debug( 'test connection success' );
        }
        var result = Ext.decode(response.responseText);
        if (debug){
          console.debug('connection test result:');
          console.debug(result);
        }
        this.showResult(result);
      },
      failure: function(){
        this.el.unmask();
        if ( debug ){
          console.debug( 'test connection failed' );
        }
        Ext.Msg.alert(Triology.cas.I18n.failedMsgText);
        form.reset();
      }
    });
  },

  specialKeyPressed: function(field, e){
    if (e.getKey() == e.ENTER) {
      var form = this.getForm();
      if ( form.isValid() ){
        this.testConnection();
      }
    }
  }

});

Triology.cas.ConnectionTestWindow = Ext.extend(Ext.Window,{

  data: null,

  initComponent: function(){
    var form = new Triology.cas.ConnectionTestForm({
      data: this.data
    });
    form.on('cancel', function(){
      this.close();
    }, this);

    var config = {
      layout:'fit',
      width: 480,
      height: 320,
      closable: true,
      resizable: true,
      plain: true,
      border: false,
      modal: true,
      title: Triology.cas.I18n.testConnectionTitleText,
      items: [form]
    };

    Ext.apply(this, Ext.apply(this.initialConfig, config));
    Triology.cas.ConnectionTestWindow.superclass.initComponent.apply(this, arguments);
  }

});
