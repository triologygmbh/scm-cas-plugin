/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
package de.triology.scm.plugins.cas.saml11;

import com.google.common.base.Throwables;
import de.triology.scm.plugins.cas.URLConnectionFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.authentication.AttributePrincipalImpl;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.AbstractUrlBasedTicketValidator;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.AssertionImpl;
import org.jasig.cas.client.validation.TicketValidationException;
import org.opensaml.SAMLAssertion;
import org.opensaml.SAMLAttribute;
import org.opensaml.SAMLAttributeStatement;
import org.opensaml.SAMLAuthenticationStatement;
import org.opensaml.SAMLException;
import org.opensaml.SAMLIdentifierFactory;
import org.opensaml.SAMLNameIdentifier;
import org.opensaml.SAMLResponse;
import org.opensaml.SAMLStatement;
import org.opensaml.SAMLSubject;
import org.opensaml.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TicketValidator that can understand validating a SAML artifact. This includes the SOAP request/response.
 * 
 * @author Ahmed Saad, Philip Pohle, TRIOLOGY GmbH
 */
public final class Saml11TicketValidator extends AbstractUrlBasedTicketValidator {

	private static final int SOAP_BODY_15 = 15;

	/** the logger for Saml11TicketValidator. */
	private static final Logger LOGGER = LoggerFactory.getLogger(Saml11TicketValidator.class);

	/** Default value of tolerance in 'ms'. */
	private static final long TOLERANCE_DEFAULT = 1000L;

	/** Time tolerance to allow for time drifting. */
	private long tolerance = TOLERANCE_DEFAULT;

	/** SOAP Template to send. */
	private static final String SOAP_TEMPLATE_TO_SEND = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\""
	    + "http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body>"
	    + "<samlp:Request xmlns:samlp=\"urn:oasis:names:tc:SAML:1.0:protocol\"  MajorVersion=\"1\""
	    + " MinorVersion=\"1\" RequestID=\"%s\" IssueInstant=\"%s\"><samlp:AssertionArtifact>%s"
	    + "</samlp:AssertionArtifact></samlp:Request></SOAP-ENV:Body></SOAP-ENV:Envelope>";

    private final URLConnectionFactory connectionFactory;
    
    /** saml validate url suffix **/
    private static final String URL_SUFFIX = "samlValidate";
    
	/**
     * @param connectionFactory
	 * @param casServerUrlPrefix - Prefix of CAS Server URL
	 */
	public Saml11TicketValidator(final URLConnectionFactory connectionFactory, final String casServerUrlPrefix) {
		super(casServerUrlPrefix);
        this.connectionFactory = connectionFactory;
	}

	@Override
	protected String getUrlSuffix() {
		return URL_SUFFIX;
	}

	@Override
	protected void populateUrlAttributeMap(final Map<String, String> urlParameters) {
		final String service = urlParameters.get("service");
		urlParameters.remove("service");
		urlParameters.remove("ticket");
		urlParameters.put("TARGET", service);
	}

	@Override
	protected void setDisableXmlSchemaValidation(final boolean disabled) {
		if (disabled) {
			// according to our reading of the SAML 1.1 code, this should disable the schema checking. However, there may be a
			// couple of error messages that slip through on start up!
			XML.parserPool.setDefaultSchemas(null, null);
		}
	}

	@Override
	protected Assertion parseResponseFromServer(final String response) throws TicketValidationException {
		try {
			final String removeStartOfSoapBody = response.substring(response.indexOf("<SOAP-ENV:Body>") + SOAP_BODY_15);
			final String removeEndOfSoapBody = removeStartOfSoapBody.substring(0,
			    removeStartOfSoapBody.indexOf("</SOAP-ENV:Body>"));

			final ByteArrayInputStream byteArrayInputStream;
			if (CommonUtils.isNotBlank(getEncoding())) {
				final byte[] buf = removeEndOfSoapBody.getBytes(Charset.forName(getEncoding()));
				byteArrayInputStream = new ByteArrayInputStream(buf);
			} else {
				byteArrayInputStream = new ByteArrayInputStream(removeEndOfSoapBody.getBytes());
			}
			final SAMLResponse samlResponse = new SAMLResponse(byteArrayInputStream);

			if (!samlResponse.getAssertions().hasNext()) {
				final String errNoAssertions = "No assertions found.";
				LOGGER.error(errNoAssertions);
				throw new TicketValidationException(errNoAssertions);
			}

			for (final Iterator<?> iter = samlResponse.getAssertions(); iter.hasNext();) {
				final SAMLAssertion assertion = (SAMLAssertion) iter.next();

				if (!isValidAssertion(assertion)) {
					continue;
				}

				final SAMLAuthenticationStatement authenticationStatement = getSAMLAuthenticationStatement(assertion);

				if (authenticationStatement == null) {
					final String errNoAuthStat = "No AuthentiationStatement found in SAML Assertion.";
					LOGGER.error(errNoAuthStat);
					throw new TicketValidationException(errNoAuthStat);
				}
				final SAMLSubject subject = authenticationStatement.getSubject();

				if (subject == null) {
					final String errNoSubject = "No Subject found in SAML Assertion.";
					LOGGER.error(errNoSubject);
					throw new TicketValidationException(errNoSubject);
				}

				final SAMLAttribute[] attributes = getAttributesFor(assertion, subject);
				final Map<String, Object> personAttributes = new HashMap<String, Object>();
				for (final SAMLAttribute samlAttribute : attributes) {
					final List<?> values = getValuesFrom(samlAttribute);

					final Object value;
					if (values.size() == 1) {
						value = values.get(0);
					} else {
						value = values;
					}
					personAttributes.put(samlAttribute.getName(), value);
				}

				final AttributePrincipal principal = new AttributePrincipalImpl(subject.getNameIdentifier().getName(),
				    personAttributes);

				final Map<String, Object> authenticationAttributes = new HashMap<String, Object>();
				authenticationAttributes
				    .put("samlAuthenticationStatement::authMethod", authenticationStatement.getAuthMethod());

				return new AssertionImpl(principal, authenticationAttributes);
			}
		} catch (final SAMLException e) {
			LOGGER.error("could not parse response from server", e);
			throw new TicketValidationException(e);
		}

		final String errAsserTime = "No Assertion found within valid time range.  Either there's a replay of "
		    + "the ticket or there's clock drift. Check tolerance range, or server/client synchronization.";
		LOGGER.error(errAsserTime);
		throw new TicketValidationException(errAsserTime);
	}

	/**
	 * Is SAML-Assertion valid?
	 * 
	 * @param assertion - Assertion to validate
	 * @return SAML Assertion
	 */
	private boolean isValidAssertion(final SAMLAssertion assertion) {
		final Date notBefore = assertion.getNotBefore();
		final Date notOnOrAfter = assertion.getNotOnOrAfter();

		if (assertion.getNotBefore() == null || assertion.getNotOnOrAfter() == null) {
			LOGGER.debug("Assertion has no bounding dates. Will not process.");
			return false;
		}

		final long currentTime = getCurrentTimeInUtc().getTime();

		if (currentTime + tolerance < notBefore.getTime()) {
			LOGGER.debug("skipping assertion that's not yet valid...");
			return false;
		}

		if (notOnOrAfter.getTime() <= currentTime - tolerance) {
			LOGGER.debug("skipping expired assertion...");
			return false;
		}

		return true;
	}

	/**
	 * Gets SAML Authentication Statement.
	 * 
	 * @param assertion - SAML Assertion
	 * @return SAML Authentication Statement
	 */
	private SAMLAuthenticationStatement getSAMLAuthenticationStatement(final SAMLAssertion assertion) {
		for (final Iterator<?> iter = assertion.getStatements(); iter.hasNext();) {
			final SAMLStatement statement = (SAMLStatement) iter.next();

			if (statement instanceof SAMLAuthenticationStatement) {
				return (SAMLAuthenticationStatement) statement;
			}
		}

		return null;
	}

	/**
	 * Gets SAML Attributes.
	 * 
	 * @param assertion - SAML Assertion
	 * @param subject - SAML Subject
	 * @return SAML Attribute
	 */
	private SAMLAttribute[] getAttributesFor(final SAMLAssertion assertion, final SAMLSubject subject) {
		final List<SAMLAttribute> attributes = new ArrayList<SAMLAttribute>();
		for (final Iterator<?> iter = assertion.getStatements(); iter.hasNext();) {
			final SAMLStatement statement = (SAMLStatement) iter.next();

			if (statement instanceof SAMLAttributeStatement) {
				final SAMLAttributeStatement attributeStatement = (SAMLAttributeStatement) statement;
				// used because SAMLSubject does not implement equals
				final SAMLNameIdentifier nameIdentifier = attributeStatement.getSubject().getNameIdentifier();
				final SAMLNameIdentifier subjNameIdentifier = subject.getNameIdentifier();
				if (subjNameIdentifier.getName().equals(nameIdentifier.getName())) {
					for (final Iterator<?> iter2 = attributeStatement.getAttributes(); iter2.hasNext();) {
						attributes.add((SAMLAttribute) iter2.next());
					}
				}
			}
		}

		return attributes.toArray(new SAMLAttribute[attributes.size()]);
	}

	/**
	 * Gets values from attribute.
	 * 
	 * @param attribute - SAML Attribute
	 * @return Values from attribute
	 */
	private List<?> getValuesFrom(final SAMLAttribute attribute) {
		final List<Object> list = new ArrayList<Object>();
		for (final Iterator<?> iter = attribute.getValues(); iter.hasNext();) {
			list.add(iter.next());
		}
		return list;
	}

	/**
	 * Gets current time in UTC.
	 * 
	 * @return Current time in UTC
	 */
	private Date getCurrentTimeInUtc() {
		final Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("UTC"));
		return c.getTime();
	}

	@Override
	protected String retrieveResponseFromServer(final URL validationUrl, final String ticket) {

		String messageToSend = "";

		try {
			String identifier = SAMLIdentifierFactory.getInstance().getIdentifier();
			messageToSend = String
			    .format(SOAP_TEMPLATE_TO_SEND, identifier, CommonUtils.formatForUtcTime(new Date()), ticket);

		} catch (final SAMLException e) {
			LOGGER.error("could not retrieve response from server", e);
		}

		HttpURLConnection conn = null;

		try {
			conn = connectionFactory.createHttpConnection(validationUrl);
			if (this.hostnameVerifier != null && conn instanceof HttpsURLConnection) {
				((HttpsURLConnection) conn).setHostnameVerifier(this.hostnameVerifier);
			}
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "text/xml");
			conn.setRequestProperty("Content-Length", Integer.toString(messageToSend.length()));
			conn.setRequestProperty("SOAPAction", "http://www.oasis-open.org/committees/security");
			conn.setUseCaches(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			final DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.writeBytes(messageToSend);
			out.flush();
			out.close();

			final InputStreamReader inputStreamReader;
			if (CommonUtils.isNotBlank(getEncoding())) {
				inputStreamReader = new InputStreamReader(conn.getInputStream(), Charset.forName(getEncoding()));
			} else {
				inputStreamReader = new InputStreamReader(conn.getInputStream());
			}
			final BufferedReader in = new BufferedReader(inputStreamReader);
			final StringBuilder buffer = new StringBuilder(128);

			String line;

			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			return buffer.toString();
		} catch (final IOException e) {
			LOGGER.error("could not retrieve response from server", e);
			throw Throwables.propagate(e);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	/**
	 * Set time tolerance to allow for time drifting.
	 * 
	 * @param pTolerance in millisecond.
	 */
	public void setTolerance(final long pTolerance) {
		this.tolerance = pTolerance;
	}

	/**
	 * time tolerance to allow for time drifting.
	 * 
	 * @return time tolerance.
	 */
	public long getTolerance() {
		return tolerance;
	}
}
