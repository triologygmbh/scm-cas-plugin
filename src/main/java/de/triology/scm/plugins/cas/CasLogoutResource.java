/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.cas;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import sonia.scm.util.HttpUtil;

/**
 * Logout from CAS.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 * @author Sebastian Sdorra, TRIOLOGY GmbH
 */
@Path("cas/logout")
public class CasLogoutResource {

	/** Authentication handler. */
	private final CasAuthenticationHandler authenticationHandler;

    /** cas logout suffix **/
    private static final String CAS_LOGOUT_SUFFIX = "logout";
    
	/** the logger for CasConfigResource. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasLogoutResource.class);

	/**
	 * Constructs ...
	 * 
	 * @param pAuthenticationHandler - CAS authentication handler
	 */
	@Inject
	public CasLogoutResource(final CasAuthenticationHandler pAuthenticationHandler) {
		authenticationHandler = pAuthenticationHandler;
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Destroys the local session and return the cas logout url or null if cas 
     * is disabled.
	 * 
     * @param request http servelt request
     * 
	 * @return cas logout url
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public final String getCasLogout(@Context HttpServletRequest request) {
		LOGGER.debug("destroy local session and return cas logout url");
        
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        
        String redirectUrl;
		final CasConfig config = authenticationHandler.getConfig();
		if (config.isEnabled()) {
            redirectUrl = createCasLogoutUrl(config);
            LOGGER.debug("cas is enabled return cas logout url {}", redirectUrl);
		} else {
			redirectUrl = HttpUtil.getCompleteUrl(request);
            LOGGER.debug("cas is disable return baseurl url {}", redirectUrl);
		}
        
        return redirectUrl;
	}
    /**
     * Return cas logout url.
     * 
     * @param config cas configuration
     * 
     * @return cas logout url
     */
    private String createCasLogoutUrl(CasConfig config) {
        return HttpUtil.append(config.getCasServerUrl(), CAS_LOGOUT_SUFFIX);
    }

}
