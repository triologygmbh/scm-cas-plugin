/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.cas;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.config.ScmConfiguration;

/**
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 * @author Sebastian Sdorra, TRIOLOGY GmbH
 */
public final class CasUtil {

	/** the logger for CasUtil */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasUtil.class);

    private static final String ATTRIBUTE = CasUtil.class.getName().concat(".attributes");
    
    
    private CasUtil(){}
    
    public static void setCasAttributes(HttpServletRequest request, Map<String,Object> attributes) {
        request.setAttribute(ATTRIBUTE, attributes);
    }
    
    public static Map<String,Object> getCasAttributes(HttpServletRequest request) {
        return (Map<String,Object>) request.getAttribute(ATTRIBUTE);
    }

	/**
	 * @param request Http Servlet Request
	 * @return true when user-agent is an scm-client
	 */
	public static boolean isBrowser(final HttpServletRequest request) {
		final String userAgent = request.getHeader("User-Agent");
		LOGGER.trace("user from: {}", userAgent);
		return userAgent.startsWith("Mozilla");
	}

	/**
	 * @param url - URL
	 * @return URL without slash at the end
	 */
	public static String stripEndSlash(final String url) {
		return StringUtils.stripEnd(url, "/");
	}
    
    /**
     * Get cas service url.
     * 
     * @param config scm-manager configuration
     * 
     * @return service url
     */
    public static String getServiceUrl(ScmConfiguration config) {
        return stripEndSlash(config.getBaseUrl()) + "/";
    }

	/**
	 * Groups from request.
	 * 
     * @param config cas configuration
	 * @param request http servlet request containing the cas attributes
	 * @return Groups from request
	 */
	public static Set<String> getGroups(CasConfig config, HttpServletRequest request) 
    {
      return getGroups(config, getCasAttributes(request));
	}
    
    public static Set<String> getGroups(CasConfig config, Map<String,Object> attributes){
        Set<String> scmGroups = new HashSet<String>();
        if ( attributes != null ){
            readGroupsFromAttribute(scmGroups, attributes.get(config.getCasAttrGroup()));
        }
        return scmGroups;
    }
    
    private static void readGroupsFromAttribute(Set<String> scmGroups, Object groupsObj)
    {
		if (groupsObj != null) {
			if (groupsObj instanceof String) {
				scmGroups.add((String) groupsObj);
			} else if (groupsObj instanceof List) {
				for (String group : (List<String>) groupsObj) {
					scmGroups.add(group);
				}

			}
		}
		LOGGER.debug("user groups from CAS: {}", groupsObj);
    }
}
