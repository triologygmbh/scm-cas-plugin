/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
package de.triology.scm.plugins.cas.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.triology.scm.plugins.cas.CasAuthenticationState;
import de.triology.scm.plugins.cas.URLConnectionFactory;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import javax.servlet.http.HttpServletResponse;

/**
 * Class for authentication with CAS-REST
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
public class CasRestClient {

	/** the logger for CasRestClient. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasRestClient.class);

	private static final String CAS_V1_TICKETS = "/v1/tickets";
    
    private static final String ENCODING = "UTF-8";

	private final CasAuthenticationState state;

    private final URLConnectionFactory connectionFactory;
    
	public CasRestClient(URLConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
		state = new CasAuthenticationState();
		// IgnoreCert.disableSslVerification(); // Only for development!
	}

	/**
	 * Validate user.
	 * 
	 * @param casServerUrl
	 * @param serviceURL
	 * @param username
	 * @param password
	 * @return Validated ticket or null
     * 
	 * @throws IOException
	 */
	public String validateFromCAS(final String casServerUrl, final String serviceURL, final String username,
	    final String password) throws IOException {

		try {
			HttpsURLConnection hsu = (HttpsURLConnection) openConn(casServerUrl + CAS_V1_TICKETS);
            StringBuilder buffer = new StringBuilder();
            buffer.append("username=").append(encode(username));
            buffer.append("&password=").append(encode(password));

			OutputStreamWriter out = new OutputStreamWriter(hsu.getOutputStream());
			BufferedWriter bwr = new BufferedWriter(out);
			bwr.write(buffer.toString());
			bwr.flush();
			bwr.close();
			out.close();

			String tgt = hsu.getHeaderField("location");
			if (tgt != null && hsu.getResponseCode() == HttpServletResponse.SC_CREATED) {
                tgt = tgt.substring(tgt.lastIndexOf('/') + 1);
				LOGGER.debug("TGT is : {}", tgt);
				
				bwr.close();
				closeConn(hsu);

				String encodedServiceURL = "service=".concat(encode(serviceURL));
				LOGGER.debug("Service url is : {}", encodedServiceURL);

				String myURL = casServerUrl + CAS_V1_TICKETS + "/" + tgt;
				hsu = (HttpsURLConnection) openConn(myURL);
				out = new OutputStreamWriter(hsu.getOutputStream());
				bwr = new BufferedWriter(out);
				bwr.write(encodedServiceURL);
				bwr.flush();
				bwr.close();
				out.close();

				LOGGER.debug("Response code is: {}", hsu.getResponseMessage());

				BufferedReader isr = new BufferedReader(new InputStreamReader(hsu.getInputStream()));
				String tkt = null;
				String ticket;
				while ((ticket = isr.readLine()) != null) {
					LOGGER.debug("Ticket: {}", ticket);
					tkt = ticket;
				}
				isr.close();
				hsu.disconnect();
				state.setAuthenticateUser(true);

				return tkt;

			} else {
				state.setAuthenticateUser(false);
				state.setException("not authenticated");
				return null;
			}

		} catch (IOException ex) {
			state.setException(ex);
            LOGGER.error("error durring validation", ex);
			throw ex;
		}

	}
    
    private String encode(String value) throws UnsupportedEncodingException{
      return URLEncoder.encode(value, ENCODING);
    }

	/**
	 * Open connection to CAS.
	 * 
	 * @param urlk
	 * @return URL Connection
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private URLConnection openConn(final String urlk) throws IOException {
		HttpURLConnection connection = connectionFactory.createHttpConnection(urlk);
		connection.setRequestMethod("POST");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "text/xml");

		state.setBind(true);
		return connection;

	}

	private static void closeConn(final HttpsURLConnection c) {
		c.disconnect();
	}

	public CasAuthenticationState getState() {
		return state;
	}
}
