/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
package de.triology.scm.plugins.cas;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.plugin.ext.Extension;
import sonia.scm.user.User;
import sonia.scm.web.filter.AutoLoginModule;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.apache.shiro.authc.AuthenticationToken;
import sonia.scm.config.ScmConfiguration;
import sonia.scm.util.HttpUtil;
import sonia.scm.web.filter.AutoLoginModules;

/**
 * This class is used in the BasicAuthenticationFilter to provide the auto-login feature.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 * @author Sebastian Sdorra, TRIOLOGY GmbH
 */
@Singleton
@Extension
public class CasAuthenticationFilter implements AutoLoginModule {

	/** the logger for CasAuthenticationFilter. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasAuthenticationFilter.class);

	/** The CAS Authentication Handler. */
	private final CasAuthenticationHandler authenticationHandler;
    
    /** SCM-Manager configuration. */
    private final ScmConfiguration configuration;

    /** header to mark cas redirect **/
    private static final String HEADER_CAS_REDIRECT = "X-CAS-Redirect";
    
	/**
	 * Constructor of the authentication filter.
	 * 
	 * @param pAuthenticationHandler - The cas authentication handler.
     * @param configuration
	 */
	@Inject
	public CasAuthenticationFilter(final CasAuthenticationHandler pAuthenticationHandler, ScmConfiguration configuration) {
        authenticationHandler = pAuthenticationHandler;
        this.configuration = configuration;
	}

	/**
	 * Authenticate a user using the given request object. If the user can not be authenticated, e.g., because required
	 * headers are not set null must be returned.
	 * 
	 * @param request - The HTTP request.
	 * @param response - The HTTP response. Use only if absolutely necessary.
	 * @param subject - The subject object.
	 * @return - Return a User object or null.
	 */
	@Override
	public final User authenticate(final HttpServletRequest request, final HttpServletResponse response,
	    final Subject subject) {

        CasConfig casConfig = authenticationHandler.getConfig();
      
		CasAuthenticationContext context = new CasAuthenticationContext(configuration, casConfig);
		User user = null;
        
        String serviceUrl = CasUtil.getServiceUrl(configuration);

		if (authenticationHandler.isCasEnabled() && ! request.getRequestURI().contains("/resources")) {
            String ticket = CommonUtils.safeGetParameter(request, "ticket");
            
            if (CommonUtils.isNotBlank(ticket)) {
                LOGGER.debug("Attempting to validate ticket: {}", ticket);
                
                // record session for single sign out
                LOGGER.debug("register ticket to single sign out handler");
                authenticationHandler.getSingleSignOutHandler().recordSession(request);

                Assertion assertion = context.validateTicket(ticket);                
                if (assertion != null) {
                    Map<String, Object> attributes = context.getAttributes(assertion);
                    String username = (String) attributes.get(casConfig.getCasAttrUsername());

                    // set attributes to use it for the authentication
                    CasUtil.setCasAttributes(request, attributes);

                    user = getLoginUser(subject, request.getRemoteAddr(), username);

                    if (user != null && request.getQueryString() != null) {
                        LOGGER.debug(request.getQueryString());
                        LOGGER.debug("Redirecting after successful ticket validation.");

                        // redirect to remove the ticket from the URL.
                        sendRedirect(request, response, serviceUrl);
                    }
                }

            } else if (CasUtil.isBrowser(request)) {
                final String location = casConfig.getCasServerUrl() + "/login?service=" + serviceUrl;
                LOGGER.debug("Redirecting for authentication to: {}", location);

                sendRedirect(request, response, location);
            }
		}
		return user;
	}

	/**
	 * Sends a temporary redirect response to the client using the specified redirect location URL. This method can accept
	 * relative URLs; the servlet container must convert the relative URL to an absolute URL before sending the response
	 * to the client. If the location is relative without a leading '/' the container interprets it as relative to the
	 * current request URI. If the location is relative with a leading '/' the container interprets it as relative to the
	 * servlet container root.
	 * 
	 * @param response - Http Servlet Response
	 * @param location - the redirect location URL
	 */
	private void sendRedirect(final HttpServletRequest request, final HttpServletResponse response, final String location) {
		try {
            if (HttpUtil.isWUIRequest(request)) {
              response.setHeader(HEADER_CAS_REDIRECT, location);
              AutoLoginModules.markAsComplete(request);
              response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }  else {
              AutoLoginModules.sendRedirect(request, response, location);
            }
		} catch (IllegalStateException e) {
			LOGGER.debug("can not send redirect {}", e.getMessage());
		} catch (IOException e) {
			LOGGER.debug("can not send redirect {}", e.getMessage());
		}
	}

	/**
	 * Send a login request and get user object if he is in scm-manager authenticated.
	 * 
	 * @param subject to do login
	 * @param remoteAddr address of scm-manager
	 * @param username user name to login
	 * @return User
	 */
	private User getLoginUser(final Subject subject, final String remoteAddr, final String username) {
		LOGGER.info("Ticket validated!");
		return tryLogin(subject, createToken(username, remoteAddr));
	}
    
    /**
     * Creates a new {@link AuthenticationToken}.
     * 
     * @param username name of the user
     * @param remoteAddr remote address
     * 
     * @return authentication token
     */
    private AuthenticationToken createToken(String username, String remoteAddr){
       return new UsernamePasswordToken(username, UUID.randomUUID().toString(), remoteAddr);
    }

	/**
	 * @param subject
	 * @param token
	 * @param user
	 * @return
	 */
	private User tryLogin(final Subject subject, final AuthenticationToken token) {
        User user = null;
		try {
			subject.login(token);
			user = subject.getPrincipals().oneByType(User.class);
		} catch (AuthenticationException e) {
			LOGGER.error(e.getMessage());
		}
		return user;
	}
}
