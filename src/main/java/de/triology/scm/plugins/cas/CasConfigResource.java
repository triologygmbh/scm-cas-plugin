/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.cas;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.security.Role;
import sonia.scm.web.security.AuthenticationResult;
import sonia.scm.web.security.AuthenticationState;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import sonia.scm.config.ScmConfiguration;

/**
 * Configuration of the CAS-Authentication-Plugin.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 * @author Sebastian Sdorra, TRIOLOGY GmbH
 */
@Singleton
@Path("config/auth/cas")
public class CasConfigResource {

	/** Authentication handler. */
	private final CasAuthenticationHandler authenticationHandler;
    
    private final ScmConfiguration configuration;

	/** the logger for CasConfigResource. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasConfigResource.class);

	/**
	 * Constructs ...
	 * 
	 * @param pAuthenticationHandler - CAS authentication handler
   * @param configuration
	 */
	@Inject
	public CasConfigResource(final CasAuthenticationHandler pAuthenticationHandler, ScmConfiguration configuration) {
		if (!SecurityUtils.getSubject().hasRole(Role.ADMIN)) {
			LOGGER.warn("user has not enough privileges to configure CAS");

			throw new WebApplicationException(Status.FORBIDDEN);
		}
        this.configuration = configuration;
		authenticationHandler = pAuthenticationHandler;
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Get configuration.
	 * 
	 * @return CAS configuration
	 */
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public final CasConfig getConfig() {
		return authenticationHandler.getConfig();
	}

	// ~--- set methods ----------------------------------------------------------
	@POST
	@Path("test")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public CasAuthenticationState testConfig(final CasTestConfig testConfig) throws IOException {
		CasConfig config = testConfig.getConfig();
		CasAuthenticationContext context = new CasAuthenticationContext(configuration, config);
		AuthenticationResult ar = context.authenticate(testConfig.getUsername(), testConfig.getPassword());
		CasAuthenticationState state = context.getState();

		if ((ar != null) && (ar.getState() == AuthenticationState.SUCCESS)) {
			state.setUser(ar.getUser());
			state.setGroups(ar.getGroups());
		}

		return state;
	}

	/**
	 * Set configuration.
	 * 
	 * @param uriInfo - UriInfo
	 * @param config - CAS configuration
	 * @return Response
	 * @throws IOException - Exception
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public final Response setConfig(@Context final UriInfo uriInfo, final CasConfig config) throws IOException {
		authenticationHandler.storeConfig(config);
		return Response.created(uriInfo.getRequestUri()).build();
	}
}
