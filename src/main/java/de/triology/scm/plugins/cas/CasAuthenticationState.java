/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
package de.triology.scm.plugins.cas;

//~--- non-JDK imports --------------------------------------------------------

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import sonia.scm.user.User;

//~--- JDK imports ------------------------------------------------------------

/**
 * 
 * @author Ahmed Saad
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authentication-state")
public class CasAuthenticationState {

	/** Field description */
	private boolean authenticateUser;

	/** Field description */
	private boolean bind;

	/** Field description */
	private String exception;

	/** Field description */
	private Collection<String> groups;

	/** Field description */
	private boolean ticketValid;

	/** Only for extjs */
	private boolean success = true;

	/** Field description */
	private User user;

	/** Field description */
	private boolean userValid = false;

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Method description
	 * 
	 * @return
	 */
	public String getException() {
		return exception;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	public Collection<String> getGroups() {
		return groups;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	public boolean isAuthenticateUser() {
		return authenticateUser;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	public boolean isBind() {
		return bind;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	public boolean isTicketValid() {
		return ticketValid;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	public boolean isUserValid() {
		return userValid;
	}

	// ~--- set methods ----------------------------------------------------------

	/**
	 * Method description
	 * 
	 * @param authenticateUser
	 */
	public void setAuthenticateUser(final boolean authenticateUser) {
		this.authenticateUser = authenticateUser;
	}

	/**
	 * Method description
	 * 
	 * @param bind
	 */
	public void setBind(final boolean bind) {
		this.bind = bind;
	}

	/**
	 * Method description
	 * 
	 * @param exception
	 */
	public void setException(final String exception) {
		this.exception = exception;
	}

	/**
	 * Method description
	 * 
	 * @param exception
	 */
	public void setException(final Exception exception) {
		this.exception = exception.getMessage();
	}

	/**
	 * Method description
	 * 
	 * @param groups
	 */
	public void setGroups(final Collection<String> groups) {
		this.groups = groups;
	}

	/**
	 * Method description
	 * 
	 * @param ticketValid
	 */
	public void setTicketValid(final boolean ticketValid) {
		this.ticketValid = ticketValid;
	}

	/**
	 * Method description
	 * 
	 * @param user
	 */
	public void setUser(final User user) {
		this.user = user;
	}

	/**
	 * Method description
	 * 
	 * @param userValid
	 */
	public void setUserValid(final boolean userValid) {
		this.userValid = userValid;
	}

	/**
	 * Method description
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		StringBuilder msg = new StringBuilder();

		msg.append("Bind: ").append(String.valueOf(bind)).append("\n").append("Ticket valid: ")
		    .append(String.valueOf(ticketValid)).append("\n").append("Authenticate user: ")
		    .append(String.valueOf(authenticateUser)).append("\n\n");

		if (user != null) {
			msg.append("user: ").append(user.toString()).append("\n");
		}

		if (groups != null) {
			msg.append("groups: ").append(groups).append("\n");
		}

		if (exception != null) {
			msg.append("Exception: \n").append(exception);
		}

		return msg.toString();
	}
}
