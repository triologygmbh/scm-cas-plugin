/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.cas;

//~--- JDK imports ------------------------------------------------------------

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import sonia.scm.Validateable;
import sonia.scm.util.Util;

/**
 * @author Ahmed Saad, TRIOLOGY GmbH
 * @author Sebastian Sdorra, TRIOLOGY GmbH
 */
@XmlRootElement(name = "cas-config")
@XmlAccessorType(XmlAccessType.FIELD)
public class CasConfig implements Validateable {

	/** Field Enable CAS Authentication. */
	@XmlElement(name = "enabled")
	private boolean enabled = false;

	/** Value of tolerance in 'ms' default is 1 second. */
	@XmlElement(name = "tolerance")
	private String tolerance = "1000";

	/** Field CAS-Server URL. */
	@XmlElement(name = "casServerUrl")
	private String casServerUrl = "https://127.0.0.1/cas";

	/** Field CAS-Attribute for user name. */
	@XmlElement(name = "casAttrUsername")
	private String casAttrUsername = "username";

	/** Field CAS-Attribute for display name. */
	@XmlElement(name = "casAttrDisplayName")
	private String casAttrDisplayName = "cn";

	/** Field CAS-Attribute for mail. */
	@XmlElement(name = "casAttrMail")
	private String casAttrMail = "mail";

	/** Field CAS-Attribute for group. */
	@XmlElement(name = "casAttrGroup")
	private String casAttrGroup = "groups";
    
    /** disable verification of SSL certificates **/
    private boolean disableSSLVerification = false;
    
    /**
     * Returns {@code true} if the verification of SSL certificates is disabled.
     * 
     * @return {@code true} if verification is disabled
     */
    public boolean isDisableSSLVerification() {
        return disableSSLVerification;
    }

    /**
     * {@code true} to disable certification validation check. This is useful in 
     * development environment with self signed certificates. This will prevent 
     * the SSLHandshakeExeption. Do not use this in production environment 
     * because it will be a security risk, e.g. man-in-the-middle attacks can 
     * not be identified.
     * 
     * @param disableSSLVerification {@code true} to disable ssl verification
     */
    public void setDisableSSLVerification(boolean disableSSLVerification) {
        this.disableSSLVerification = disableSSLVerification;
    }
    
	/**
	 * Get CAS Path.
	 * 
	 * @return CAS-Server-URL
	 */
	public final String getCasServerUrl() {
		return casServerUrl;
	}

	/**
	 * Method description.
	 * 
	 * @return Tolerance in 'ms'
	 */
	public final long getTolerance() {
		return Long.valueOf(tolerance);
	}

	/**
	 * @return the casAttrUsername
	 */
	public final String getCasAttrUsername() {
		return casAttrUsername;
	}

	/**
	 * @param casAttrUsername the casAttrUsername to set
	 */
	public final void setCasAttrUsername(final String casAttrUsername) {
		this.casAttrUsername = casAttrUsername;
	}

	/**
	 * @return the casAttrDisplayName
	 */
	public final String getCasAttrDisplayName() {
		return casAttrDisplayName;
	}

	/**
	 * @param casAttrDisplayName the casAttrDisplayName to set
	 */
	public final void setCasAttrDisplayName(final String casAttrDisplayName) {
		this.casAttrDisplayName = casAttrDisplayName;
	}

	/**
	 * @return the casAttrMail
	 */
	public final String getCasAttrMail() {
		return casAttrMail;
	}

	/**
	 * @param casAttrMail the casAttrMail to set
	 */
	public final void setCasAttrMail(final String casAttrMail) {
		this.casAttrMail = casAttrMail;
	}

	/**
	 * @return the casAttrGroup
	 */
	public final String getCasAttrGroup() {
		return casAttrGroup;
	}

	/**
	 * @param casAttrGroup the casAttrGroup to set
	 */
	public final void setCasAttrGroup(final String casAttrGroup) {
		this.casAttrGroup = casAttrGroup;
	}

	/**
	 * Set CAS Server URL.
	 * 
	 * @param casServerUrl - URL of CAS
	 */
	public final void setCasServerUrl(final String casServerUrl) {
		this.casServerUrl = CasUtil.stripEndSlash(casServerUrl);
	}

	/**
	 * Set Tolerance for CAS.
	 * 
	 * @param tolerance - Tolerance in 'ms'
	 */
	public final void setTolerance(final String tolerance) {
		this.tolerance = tolerance;
	}

	/**
	 * @param enabled - the enabled to set
	 */
	public final void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns true if CAS enabled.
	 * 
	 * @return true if CAS enabled.
	 */
	public final boolean isEnabled() {
		return enabled;
	}

	@Override
	public final boolean isValid() {
		return isValid(casServerUrl, tolerance, casAttrGroup, casAttrMail, casAttrDisplayName, casAttrUsername);
	}

	/**
	 * @param pFields - Fields to validate
	 * @return true if fields are valid
	 */
	private boolean isValid(final String... pFields) {
		boolean valid = true;

		for (String field : pFields) {
			if (Util.isEmpty(field)) {
				valid = false;

				break;
			}
		}

		return valid;
	}
}
