/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */



package de.triology.scm.plugins.cas;

//~--- non-JDK imports --------------------------------------------------------

import com.google.inject.Inject;

import org.jasig.cas.client.session.SessionMappingStorage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.plugin.ext.Extension;

//~--- JDK imports ------------------------------------------------------------

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Sebastian Sdorra <sebastian.sdorra@triology.de>
 */
@Extension
public class CasSingleSignOutSessionListener implements HttpSessionListener
{

  /** Field description */
  private static final Logger LOGGER =
    LoggerFactory.getLogger(CasSingleSignOutSessionListener.class);

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs ...
   *
   *
   * @param authenticationHandler
   */
  @Inject
  public CasSingleSignOutSessionListener(
    CasAuthenticationHandler authenticationHandler)
  {
    this.authenticationHandler = authenticationHandler;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   *
   *
   * @param event
   */
  @Override
  public void sessionCreated(HttpSessionEvent event)
  {

    // do nothing
  }

  /**
   * Method description
   *
   *
   * @param event
   */
  @Override
  public void sessionDestroyed(HttpSessionEvent event)
  {

    final HttpSession session = event.getSession();

    LOGGER.debug("remove session {} from mapping storage", session.getId());
    getSessionMappingStorage().removeBySessionById(session.getId());
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @return
   */
  private SessionMappingStorage getSessionMappingStorage()
  {
    return authenticationHandler.getSingleSignOutHandler()
      .getSessionMappingStorage();
  }

  //~--- fields ---------------------------------------------------------------

  /** Field description */
  private final CasAuthenticationHandler authenticationHandler;
}
