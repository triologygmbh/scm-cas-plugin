/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */



package de.triology.scm.plugins.cas;

//~--- non-JDK imports --------------------------------------------------------

import com.google.inject.Inject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.jasig.cas.client.ssl.AnyHostnameVerifier;

//~--- JDK imports ------------------------------------------------------------

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author Sebastian Sdorra <sebastian.sdorra@triology.de>
 */
public final class URLConnectionFactory
{
  
  private final CasConfig casConfig;

  /**
   * Constructs ...
   *
   * @param casConfig
   */
  @Inject
  public URLConnectionFactory(CasConfig casConfig) 
  {
    this.casConfig = casConfig;
  }

  //~--- methods --------------------------------------------------------------

  public HttpURLConnection createHttpConnection(URL url) throws IOException {
      URLConnection connection = url.openConnection();
      if ( casConfig.isDisableSSLVerification() && connection instanceof HttpsURLConnection ) {
         disableSslVerification((HttpsURLConnection) connection);
      }
      if ( ! (connection instanceof HttpURLConnection) ){
        throw new IOException("url is not a http connection");
      }
      return (HttpURLConnection) connection;      
  }
  
  public HttpURLConnection createHttpConnection(String urlString) throws IOException {
      URL url = new URL(urlString);
      return createHttpConnection(url);
  }
  
  
  private void disableSslVerification(HttpsURLConnection connection) throws IOException
  {
    try
    {
      connection.setHostnameVerifier(new AnyHostnameVerifier());

      TrustManager[] trustManagers = new TrustManager[] {
        new TrustAllTrustManager() 
      };
      
      SSLContext sc = SSLContext.getInstance("SSL");

      sc.init(null, trustManagers, new java.security.SecureRandom());
      connection.setSSLSocketFactory(sc.getSocketFactory());
    }
    catch (KeyManagementException ex)
    {
      throw new IOException("could not disable ssl verification", ex);
    }
    catch (NoSuchAlgorithmException ex)
    {
      throw new IOException("could not disable ssl verification", ex);
    }
  }

  //~--- inner classes --------------------------------------------------------

  /**
   * Class description
   *
   *
   * @version        Enter version here..., 14/10/08
   * @author         Enter your name here...    
   */
  private static class TrustAllTrustManager implements X509TrustManager
  {

    /**
     * Method description
     *
     *
     * @param xcs
     * @param string
     *
     * @throws CertificateException
     */
    @Override
    public void checkClientTrusted(X509Certificate[] xcs, String string)
      throws CertificateException
    {

      // do nothing
    }

    /**
     * Method description
     *
     *
     * @param xcs
     * @param string
     *
     * @throws CertificateException
     */
    @Override
    public void checkServerTrusted(X509Certificate[] xcs, String string)
      throws CertificateException
    {

      // do nothing
    }

    //~--- get methods --------------------------------------------------------

    /**
     * Method description
     *
     *
     * @return
     */
    @Override
    public X509Certificate[] getAcceptedIssuers()
    {
      return null;
    }
  }
}
