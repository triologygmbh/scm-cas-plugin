/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.cas;

//~--- JDK imports ------------------------------------------------------------

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Ahmed Saad
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "cas-test-config")
public class CasTestConfig {

	/** Field description */
	@XmlElement(name = "cas-config")
	private CasConfig config;

	/** Field description */
	private String password;

	/** Field description */
	private String username;

	/**
	 * Constructs new CASTestConfig. This constructor is only visible for JAXB.
	 * 
	 */
	public CasTestConfig() {
	}

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param username
	 * @param password
	 * @param config
	 */
	public CasTestConfig(final CasConfig config, final String username, final String password) {
		this.config = config;
		this.username = username;
		this.password = password;
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Method description
	 * 
	 * 
	 * @return
	 */
	public CasConfig getConfig() {
		return config;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @return
	 */
	public String getUsername() {
		return username;
	}
}
