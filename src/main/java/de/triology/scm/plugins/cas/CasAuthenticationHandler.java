/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
package de.triology.scm.plugins.cas;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.SCMContextProvider;
import sonia.scm.config.ScmConfiguration;
import sonia.scm.plugin.ext.Extension;
import sonia.scm.store.Store;
import sonia.scm.store.StoreFactory;
import sonia.scm.user.User;
import sonia.scm.web.security.AuthenticationHandler;
import sonia.scm.web.security.AuthenticationResult;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import org.jasig.cas.client.session.SingleSignOutHandler;

/**
 * The authentication handler to support the auto login.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 * @author Sebastian Sdorra, TRIOLOGY GmbH
 */
@Singleton
@Extension
public class CasAuthenticationHandler implements AuthenticationHandler {

	/** The authentication type. */
	public static final String TYPE = "cas";

	/** The type used for the store. */
	public static final String STORETYPE = "cas_plugin";

	/** the logger for AutoLoginAuthenticationHandler. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasAuthenticationHandler.class);

	/** The configuration of the plugin. */
	private CasConfig config;

	/** The store of the configuration. */
	private final Store<CasConfig> store;

	/** The main configuration object for SCM-Manager. */
	private final ScmConfiguration scmConfiguration;
    
    /** handler for cas SingleSignOut requests **/
    private final SingleSignOutHandler singleSignOutHandler = new SingleSignOutHandler();

	/**
	 * Constructor.
	 * 
	 * @param storeFactory - The factory to get the store.
	 * @param pScmConfiguration - The main configuration object for SCM-Manager.
	 */
	@Inject
	public CasAuthenticationHandler(final StoreFactory storeFactory,
	    final ScmConfiguration pScmConfiguration) {
		scmConfiguration = pScmConfiguration;
		store = storeFactory.getStore(CasConfig.class, STORETYPE);
	}

	/**
	 * Initialize the AutoLoginAuthenticationHandler.
	 * 
	 * @param context SCMContextProvider
	 */
	@Override
	public final void init(final SCMContextProvider context) {
		config = store.get();

		if (config == null) {
			config = new CasConfig();
		}
        LOGGER.debug("initialize single sign out handler");
        singleSignOutHandler.init();
	}

	/**
	 * Close the AutoLoginAuthenticationHandler.
	 * 
	 * @throws IOException exception
	 */
	@Override
	public void close() throws IOException {
	}

	/**
	 * Get the type of the AutoLoginAuthenticationHandler.
	 * 
	 * @return TYPE
	 */
	@Override
	public final String getType() {
		return TYPE;
	}

    /**
     * Returns the single sign out handler.
     * 
     * @return single sign out handler
     */
    public SingleSignOutHandler getSingleSignOutHandler()
    {
      return singleSignOutHandler;
    }

	@Override
	public final AuthenticationResult authenticate(final HttpServletRequest request, final HttpServletResponse response,
	    final String username, final String password) {

		// The request originated from the CasAuthenticationFilter...
		AuthenticationResult result = null;
		if (isCasEnabled()) {
            if (CasUtil.isBrowser(request)){
                // Create user from CAS attributes
                User casUser = createCasUser(username, request);

                if (casUser != null) {
                    Set<String> groups = CasUtil.getGroups(config, request);
                    result = new AuthenticationResult(casUser, groups);
                    LOGGER.debug("user {} successfully authenticated by cas plugin", username);
                } 
            }
            else 
            {
                CasAuthenticationContext context = new CasAuthenticationContext(scmConfiguration, config);
                result = context.authenticate(username, password);
			}
		}

		if (result == null) {
			result = AuthenticationResult.NOT_FOUND;

			LOGGER.debug("user {} not authenticated by cas plugin ", username);
		}
		return result;
	}

	/**
	 * @return true if CAS-Configuration is enabled and valid
	 */
	public boolean isCasEnabled() {
		return config.isEnabled() && config.isValid();
	}

	/**
	 * Set the plugin configuration and store it in the store.
	 * 
	 * @param config the plugin configuration.
	 */
	public final synchronized void storeConfig(final CasConfig config) {
        config.setCasServerUrl(CasUtil.stripEndSlash(config.getCasServerUrl()));
		this.config = config;
		store.set(config);
	}

	/**
	 * Get the plugin configuration.
	 * 
	 * @return The plugin configuration.
	 */
	public final CasConfig getConfig() {
		return config;
	}

	/**
	 * Create a new user and save it in the user database.
	 * 
	 * @param username - The user name.
	 * @param request - Http Servlet Request
	 * @return The user object.
	 */
	private User createCasUser(final String username, final HttpServletRequest request) {
      
        User user = null;
        Map<String,Object> attributes = CasUtil.getCasAttributes(request);
      
        if (attributes != null){
            String usernameAttr = (String) attributes.get(config.getCasAttrUsername());
            String dispnameAttr = (String) attributes.get(config.getCasAttrDisplayName());
            String mailAttr = (String) attributes.get(config.getCasAttrMail());


            // is this really required ?
            if (usernameAttr.equals(username)) {
                user = CasAuthenticationContext.createUser(username, dispnameAttr, mailAttr);
            }
        } else {
          LOGGER.warn("no cas attributes found in request");
        }
		return user;
	}
}
