/**
 * Copyright (c) 2014, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.cas;

//~--- non-JDK imports --------------------------------------------------------

import com.google.common.base.Strings;
import java.util.Map;
import java.util.Set;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.TicketValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.user.User;
import sonia.scm.web.security.AuthenticationResult;
import de.triology.scm.plugins.cas.client.CasRestClient;
import de.triology.scm.plugins.cas.saml11.Saml11TicketValidator;

import sonia.scm.config.ScmConfiguration;

//~--- JDK imports ------------------------------------------------------------

/**
 * 
 * @author Ahmed Saad
 * @author Sebastian Sdorra
 */
public class CasAuthenticationContext {

	/** the logger for CASContext */
	private static final Logger LOGGER = LoggerFactory.getLogger(CasAuthenticationContext.class);

	/** Field description */
	private final CasConfig casConfig;
    
    private final ScmConfiguration configuration;

	/** Field description */
	private CasAuthenticationState state;
    
    private final URLConnectionFactory connectionFactory;

	// ~--- constructors ---------------------------------------------------------

	/**
	 * Constructs ...
	 * 
	 * 
   * @param configuration
	 * @param casConfig
	 */
	public CasAuthenticationContext(ScmConfiguration configuration, CasConfig casConfig){
        this.configuration = configuration;
		this.casConfig = casConfig;
		this.state = new CasAuthenticationState();
        this.connectionFactory = new URLConnectionFactory(casConfig);
	}

	// ~--- methods --------------------------------------------------------------
	/**
	 * Validate the ticket.
	 * 
	 * @param ticket Ticket to validate
	 * @return Assertion and null if ticket not valid
	 */
	protected Assertion validateTicket(final String ticket) {
		Saml11TicketValidator ticketValidator = new Saml11TicketValidator(connectionFactory, casConfig.getCasServerUrl());
		ticketValidator.setTolerance(casConfig.getTolerance());
		Assertion assertion = null;
		final String baseUrl = CasUtil.getServiceUrl(configuration);

		try {
			assertion = ticketValidator.validate(ticket, baseUrl);

		} catch (TicketValidationException e) {
			state.setException(e);
			LOGGER.debug("Ticket validation failed", e);
		}
		return assertion;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param username
	 * @param password
	 * 
	 * @return
	 */
	public AuthenticationResult authenticate(final String username, final String password) {
		AuthenticationResult result = AuthenticationResult.NOT_FOUND;
		String ticket = authenticateUser(username, password);
		if (ticket != null) {
			Assertion assertion = validateTicket(ticket);
			if (assertion != null) {
				state.setTicketValid(true);
				Map<String, Object> attributes = getAttributes(assertion);

				// collect groups
				Set<String> groups = CasUtil.getGroups(casConfig, attributes);
				state.setGroups(groups);

				// Create user from CAS attributes
				User casUser = createCasUser(attributes);

				if (casUser != null) {

					result = new AuthenticationResult(casUser, groups);
					LOGGER.debug("user {} successfully authenticated by cas plugin with groups[{}]", username, groups);
				}
			}
		}
		return result;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @return
	 */
	public CasAuthenticationState getState() {
		return state;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param userDN
	 * @param password
	 * 
	 * @return
	 */
	private String authenticateUser(final String username, final String password) {
		String ticket = null;
		final String baseUrl = CasUtil.getServiceUrl(configuration);
		try {
			CasRestClient client = new CasRestClient(connectionFactory);
			ticket = client.validateFromCAS(casConfig.getCasServerUrl(), baseUrl, username, password);
			state = client.getState();
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
		return ticket;
	}

	/**
	 * Create a new user and save it in the user database.
	 * 
	 * @param attribute - Attributes from CAS
	 * @return The user object.
	 */
	private User createCasUser(final Map<String, Object> attribute) {

		String usernameAttr = (String) attribute.get(casConfig.getCasAttrUsername());
		String dispnameAttr = (String) attribute.get(casConfig.getCasAttrDisplayName());
		String mailAttr = (String) attribute.get(casConfig.getCasAttrMail());

		User user = createUser(usernameAttr, dispnameAttr, mailAttr);
		if (user.isValid()) {
			state.setUserValid(true);
		}
		return user;
	}

	/**
	 * @param username
	 * @param displayName
	 * @param mail
	 * @return
	 */
	public static User createUser(final String username, final String displayName, final String mail) {
		User user = new User();
		user.setName(username);
        if (Strings.isNullOrEmpty(displayName)){
          user.setDisplayName(username);
        } else {
          user.setDisplayName(displayName);
        }
		user.setMail(mail);
		user.setPassword(null);
		user.setType(CasAuthenticationHandler.TYPE);

		LOGGER.debug("user {} successfully created by CAS plugin", username);
		return user;
	}

	/**
	 * Gets attributes from assertion.
	 * 
	 * @param assertion assertion of CAS
	 * @return a map of attributes
	 */
	protected Map<String, Object> getAttributes(final Assertion assertion) {
		// get the user's credentials
		AttributePrincipal principal = assertion.getPrincipal();

		// get the released attributes, e.g., LDAP group membership
		Map<String, Object> pAttributes = principal.getAttributes();
		if (pAttributes.isEmpty()) {
			pAttributes.put(casConfig.getCasAttrUsername(), principal.getName());
		}

		return pAttributes;
	}
}
